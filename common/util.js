var Excel = require('exceljs');
var tmp = require('tmp');

var commonUtil = {

  getEmptyExcel() {
    var workbook = new Excel.Workbook();
    workbook.creator = 'System';
    workbook.created = new Date();
    return workbook;
  },

  getTempFile(prefix, sufffix) {
    tmp.setGracefulCleanup();
    var tmpFile = tmp.fileSync({
      prefix: prefix,
      postfix: sufffix
    });
    return tmpFile;
  }
};

module.exports = commonUtil;