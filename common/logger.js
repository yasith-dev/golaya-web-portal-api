const winston = require("winston");

const logFormat = winston.format.printf(({ timestamp, level, message }) => {
  return `${timestamp} ${level.toUpperCase()} ${message}`;
});
const logger = new winston.createLogger({
  transports: [
      new winston.transports.Console({
          level: 'debug',
          format: winston.format.combine(
            winston.format.timestamp({format: 'YYYY-MM-DD HH:mm:ss.SSSSSS'}),
            winston.format.padLevels(),
            logFormat
          )
      })
  ]
});
 
//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
// 
// if (process.env.NODE_ENV === 'production') {
//   logger.add(new winston.transports.File({ filename: 'error.log', level: 'error', format: winston.format.json() }));
//   logger.add(new winston.transports.File({ filename: 'combined.log', format: winston.format.json() }));
// }

module.exports = logger;{format: 'YYYY-MM-DD HH:mm:ss'}