var express = require('express');
// var paginate = require('express-paginate');
var salesRouter = express.Router();
var Sales = require('../model/Sales');
var User = require('../model/User');
var commonUtil = require('../common/util');
var PdfPrinter = require('pdfmake');
var fs = require('fs');
const AuthVerify = require('../authentication/authVerify');
const xlsx = require('xlsx')
var db = require('../db/db_connection');

salesRouter.get('/', AuthVerify, function(req, res, next){
  from = req.query.from;
  to = req.query.to;
  pType = req.query.pType;
  Sales.getInDateRange(from, to, pType, function(err, rows){
    if(err){
      console.error(err);
    }else{
      Sales.getInDateRangeGraphData(from, to, pType, function(err, rows2) {
        if(err){
          console.error(err);
        } else {
          var resp = {
            sales: rows,
            salesGraph: rows2
          };
          res.send(resp);
        }
      });
      
    }
    
  });
});

// salesRouter.get('/jsontoxl', function(req, res, next){
//   var rawFile = fs.readFileSync('order.json')//dir of your json file as param
//   var raw = JSON.parse(rawFile)
//   var files  = []
//   for (each in raw){
//     files.push(raw[each])
//     }  
//    var obj = files.map((e) =>{
//         return e
//        })

//    var newWB = xlsx.utils.book_new()

//    var newWS = xlsx.utils.json_to_sheet(obj)

//    xlsx.utils.book_append_sheet(newWB,newWS,"name")//workbook name as param

//    xlsx.writeFile(newWB,"Sample-Sales-Data.xlsx")//file name as param
//    res.send("success")
// });

salesRouter.get('/all', AuthVerify, function(req, res, next){
  Sales.getAll(function(err, rows){
    if(err){
      console.log(err);
    }else{
      // var rowCount = rows.length;
      // var pageCount = Math.ceil(rowCount / req.query.limit);
      var resp = {
        sales: rows
      };
      res.send(resp);
    }
  });
});

salesRouter.get('/current', AuthVerify, function(req, res, next){
  Sales.getCurrent(function(err, rows){
    if(err){
      console.log(err);
    }else{
      // var rowCount = rows.length;
      // var pageCount = Math.ceil(rowCount / req.query.limit);
      var resp = {
        sales: rows
      };
      res.send(resp);
    }
  });
});

salesRouter.get('/discounted', AuthVerify, function(req, res, next){
  Sales.getDiscounted(function(err, rows){
    if(err){
      console.log('Error fetching data from DB');
      console.error(err);
    }else{
      var resp = {
        sales: rows
      };
      res.send(resp);
    }
  });
});

salesRouter.get('/day', AuthVerify, function(req, res, next){
  const date = req.query.date;
  Sales.getDay(date, function(err, rows){
    if(err){
      console.error(err);
    }else{
      console.log(rows)
      res.send(rows);
    }
  });
});

salesRouter.get('/month', AuthVerify, function(req, res, next){
  const from = req.query.from;
  const to = req.query.to;
  Sales.getMonth(from, to, function(err, rows){
    if(err){
      console.error(err);
    }else{
      console.log(rows)
      res.send(rows);
    }
  });
});

salesRouter.get('/year', AuthVerify, function(req, res, next){
  const from = req.query.from;
  const to = req.query.to;
  Sales.getYear(from, to, function(err, rows){
    if(err){
      console.error(err);
    }else{
      console.log(rows)
      res.send(rows);
    }
  });
});

salesRouter.get('/data', AuthVerify, function(req, res, next){
  const date = req.query.date;
  Sales.getTotalSales(date, function(err, rows){
    if(err){
      console.log(err);
    }else{
      Sales.getTopItems(date, function(err, rows2) {
        if(err){
          console.log(err);
        }else{
          Sales.getTopCategories(date, function(err, rows3) {
            if(err){
              console.log(err);
            }else{
              var resp = {
                sales: rows,
                items: rows2,
                categories: rows3
              };
              res.send(resp);
            }
          });
        }
      });
    }
  });
});

salesRouter.get('/download/report/excel', function(req, res, next){
  const username = req.query.username;
  from = req.query.from;
  to = req.query.to;
  pType = req.query.pType;
  Sales.getInDateRange(from, to, pType, function(err, rows){
    if(err){
      console.error(err);
    }else{
      User.location(username, function(err, location) {
        if(err){
          console.error(err);
        }else{
          var workbook = commonUtil.getEmptyExcel();
          sheet = workbook.addWorksheet('Sales Report');
          file = commonUtil.getTempFile('sales-report-','.xlsx');

          sheet.columns = [
            {id: 'invoice_no', width: 30},
            {id: 'sub_total', width: 20},
            {id: 'discount', width: 20},
            {id: 'grand_total', width: 20},
            {id: 'cost_total', width: 20},
            {id: 'pos', width: 30},
            {id: 'timestamp', width: 30}
          ];

          // set title
          sheet.mergeCells('A2:D2');
          sheet.getCell('A2').value = "Sales Report of "+location[0]['name'];
          sheet.getCell('A2').font = {size: 14, name:'Calibri', bold:true};
          sheet.getCell('A2').alignment = {vertical:'middle'};
          sheet.getRow(2).height = 30;
          sheet.addRow(2).values = ['Generated on:',new Date()];
          sheet.addRow(2).values = ['Address:', location[0]['address']];
          sheet.addRow(2).values = ['Email:',location[0]['email']];
          sheet.addRow(2).values = ['Contact No', location[0]['contact_number']];
          sheet.addRow(4);
          

          // set column headers
          sheet.addRow(4).values = ['Invoice No','Sub Total (Rs)','Discount (Rs)','Grand Total (Rs)', 'Cost Total (Rs)', 'POS No.', 'Timestamp'];
          sheet.getRow(8).font = {bold:true, name:'Calibri'};

          let orders = 0, grossTotal = 0, netTotal = 0;
          for(var i=0; i<rows.length; i++){
            var row = rows[i];
            var discount = 0;
            if(row.discount_is_percentage == 1){
              discount = (row.sub_total - row.items_discount_total)*row.discount_amount/100;
            }else{
              discount = row.discount_amount;
            }
            discount += row.items_discount_total;
            tem_row = [
              row.invoice_no, row.sub_total.toFixed(2), discount.toFixed(2), row.grand_total.toFixed(2), row.cost_total.toFixed(2), row.pos_no,
              row.timestamp
            ];
            sheet.addRow(tem_row);
            orders = orders + 1;
            grossTotal = grossTotal + row.sub_total;
            netTotal = netTotal + row.grand_total;
          }

          sheet.addRow(4);
          sheet.addRow(4).values = ["", "", "", "", "", "Summary", ];
          sheet.addRow(4).values = ["", "", "", "", "", "No. of Orders:", orders]
          sheet.addRow(4).values = ["", "", "", "", "", "Gross Total:", grossTotal.toFixed(2)]
          sheet.addRow(4).values = ["", "", "", "", "", "Net Total:", netTotal.toFixed(2)]


          workbook.xlsx.writeFile(file.name).then(function(){
            console.log('created file : '+file.name);
            res.sendFile(file.name,function(err){
            });
          });
        }
      });
    }
  });
});

salesRouter.get('/download/report/pdf', function(req, res, next){
  const username = req.query.username;
  from = req.query.from;
  to = req.query.to;
  pType = req.query.pType;
  Sales.getInDateRange(from, to, pType, function(err, rows){
    if(err){
      console.error(err);
    }else{
      User.location(username, function(err, location) {
        if(err){
          console.error(err);
        }else{
          var fonts = {
            Roboto: {
              normal: 'fonts/Roboto-Regular.ttf',
              bold: 'fonts/Roboto-Medium.ttf',
              italics: 'fonts/Roboto-Italic.ttf',
              bolditalics: 'fonts/Roboto-MediumItalic.ttf'
            }
          };
          
          var printer = new PdfPrinter(fonts);
    
          var bdy = [
            [{text: 'Invoice No',style: 'tableHeader'},
            {text: 'Sub Total (Rs)', style: 'tableHeader'},
            {text: 'Discount (Rs)', style: 'tableHeader'},
            {text: 'Grand Total (Rs)', style: 'tableHeader'},
            {text: 'Cost Total (Rs)', style: 'tableHeader'},
            {text: 'POS No.', style: 'tableHeader'},
            {text: 'Timestamp', style: 'tableHeader'}]
          ]
    
          let orders = 0, grossTotal = 0, netTotal = 0;
          if (rows.length > 0) {
            for(var i=0; i<rows.length; i++){
              var row = rows[i];
              var discount = 0;
              if(rows[i].discount_is_percentage == 1){
                discount = (rows[i].sub_total - rows[i].items_discount_total)*rows[i].discount_amount/100;
              }else{
                discount = rows[i].discount_amount;
              }
              discount += rows[i].items_discount_total;
              console.log(rows[i].timestamp.toISOString().slice(0, 10))
              bdy.push([
                rows[i].invoice_no,
                {text: rows[i].sub_total.toFixed(2), alignment: 'right'},
                {text: discount.toFixed(2), alignment: 'right'},
                {text: rows[i].grand_total.toFixed(2), alignment: 'right'},
                {text: rows[i].cost_total.toFixed(2), alignment: 'right'},
                rows[i].pos_no,
                rows[i].timestamp.toISOString().slice(0, 10)]);

                orders = orders + 1;
                grossTotal = grossTotal + row.sub_total;
                netTotal = netTotal + row.grand_total;
            }
          } else {
            bdy.push(["no data", "no data", "no data", "no data", "no data", "no data", "no data"]);
          }
    
          var bdy2 = [
            [{text: 'Summary', style: 'tableHeader'}, ""],
            ["No. of Orders", {text: orders, alignment: 'right'}],
            ["Gross Total", {text: grossTotal.toFixed(2), alignment: 'right'}],
            ["Net Total", {text: netTotal.toFixed(2), alignment: 'right'}]
          ]
          
    
          var docDefinition = {
            content: [
              {
                text: 'Sales Report of '+location[0]['name'],
                style: 'header',
                margin: [ 0, 0, 0, 10 ]
              },
              'Generated on: '+new Date(),
              'Address: '+location[0]["address"],
              'Email: '+location[0]["email"],
              {text: 'Contact No: '+location[0]["contact_number"], margin: [ 0, 0, 0, 10 ]},
              {
                style: 'tableExample',
                table: {
                  headerRows: 1,
                  body: bdy
                },
                layout: {
                  hLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 2 : 1;
                  },
                  vLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 2 : 1;
                  },
                  hLineColor: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
                  },
                  vLineColor: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
                  }
                }
              },
              {
                style: 'tableExample',
                table: {
                  headerRows: 1,
                  body: bdy2
                },
                layout: {
                  hLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 2 : 1;
                  },
                  vLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 2 : 1;
                  },
                  hLineColor: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
                  },
                  vLineColor: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
                  }
                }
              }
            ],
            styles: {
              header: {
                fontSize: 15,
                bold: true
              },
              tableExample: {
                margin: [0, 5, 0, 15]
              },
              tableHeader: {
                bold: true,
                fontSize: 13,
                color: 'black'
              }
            }
          };
    
          // Building the PDF
          var pdfDoc = printer.createPdfKitDocument(docDefinition);
          let chunks = [];
    
          pdfDoc.on('data', (chunk) => {
            chunks.push(chunk);
          });
        
          pdfDoc.on('end', () => {
            const result = Buffer.concat(chunks);
            res.setHeader('Content-Type', 'application/pdf');
            res.send(result);
          });
          
          pdfDoc.end();
        }
      });
    }
  });
});

module.exports=salesRouter;

