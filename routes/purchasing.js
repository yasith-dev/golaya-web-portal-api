const express = require('express');
const purchaseRouter = express.Router();
const PurchaseHistory = require('../model/PurchaseHistory');
var User = require('../model/User');
const commonUtil = require('../common/util');
var PdfPrinter = require('pdfmake');
var fs = require('fs');
const AuthVerify = require('../authentication/authVerify');

purchaseRouter.get('/', AuthVerify, (req, res, next) => {
  from = req.query.from;
  to = req.query.to;
  category = req.query.category;
  subCategory = req.query.subCategory;
  supplier = req.query.supplier;
  PurchaseHistory.getInDateRange(from, to, category, subCategory, supplier, (err, rows) => {
    if(err){
      console.error(err);
    }else{
      PurchaseHistory.getInDateRangeGraphData(from, to, category, subCategory, supplier, function(err, rows2) {
          if(err){
            console.error(err);
          } else {
            var resp = {
              rows: rows,
              purchasingGraph: rows2
            };
            res.send(resp);
          }
        });
        
    }
  });
});

purchaseRouter.get('/all', AuthVerify, (req, res, next) => {
  PurchaseHistory.getAll((error, rows) => {
    if(error){
      console.log(error);
    }else{
      res.json({
        rows: rows
      });
    }
  });
});

purchaseRouter.get('/lastMonth/value', AuthVerify, (req, res, next) => {
  PurchaseHistory.getLastMonthPurchaseValue((error, rows) => {
    if(error){
      console.log(error);
    }else{
      console.log(rows);
      res.json({
        rows: rows
      });
    }
  });
});

purchaseRouter.get('/download/report/excel', function(req, res, next){
  const username = req.query.username;
  from = req.query.from;
  to = req.query.to;
  category = req.query.category;
  subCategory = req.query.subCategory;
  supplier = req.query.supplier;
  PurchaseHistory.getInDateRange(from, to, category, subCategory, supplier, function(err, rows){
    if(err){
      console.error(err);
    }else{
      User.location(username, function(err, location) {
        var workbook = commonUtil.getEmptyExcel();
        sheet = workbook.addWorksheet('Purchasing Report');
        file = commonUtil.getTempFile('purchasing-report-','.xlsx');

        // set columns
        sheet.columns = [
          {id: 'name', width: 30},
          {id: 'brand', width: 20},
          {id: 'category', width: 20},
          {id: 'sub_category', width: 20},
          {id: 'cost_price', width: 20},
          {id: 'qty', width: 20},
          {id: 'total', width: 20},
          {id: 'time_stamp', width: 20}
        ];

        // set title
        sheet.mergeCells('A2:D2');
        sheet.getCell('A2').value = "Purchasing Report of "+location[0]['name'];
        sheet.getCell('A2').font = {size: 14, name:'Calibri', bold:true};
        sheet.getCell('A2').alignment = {vertical:'middle'};
        sheet.getRow(2).height = 30;
        sheet.addRow(2).values = ['Generated on:',new Date()];
        sheet.addRow(2).values = ['Address:', location[0]['address']];
        sheet.addRow(2).values = ['Email:',location[0]['email']];
        sheet.addRow(2).values = ['Contact No', location[0]['contact_number']];
        sheet.addRow(4);

        // set column headers
        sheet.addRow(4).values = ['Item Name','Brand','Category','Sub Category', 'Cost Price (Rs)', 'Quantity', 'Total (Rs)', 'Timestamp'];
        sheet.getRow(8).font = {bold:true, name:'Calibri'};

        let items = 0, total = 0
        for(let i=0; i<rows.length; i++){
          let row = rows[i];
          tem_row = [
            row.name, row.brand, row.category, row.sub_category, row.cost_price.toFixed(2), parseFloat(row.qty.toFixed(3)),
            row.total.toFixed(2), row.time_stamp
          ];
          sheet.addRow(tem_row);

          items = items + 1;
          total = total + row.total;
        }

        sheet.addRow(4);
        sheet.addRow(4).values = ["", "", "", "", "", "", "Summary", ];
        sheet.addRow(4).values = ["", "", "", "", "", "", "No. of Items:", items]
        sheet.addRow(4).values = ["", "", "", "", "", "", "Total:", total.toFixed(2)]

        workbook.xlsx.writeFile(file.name).then(function(){
          console.log('created file : '+file.name);
          res.sendFile(file.name,function(err){
          });
        });
      });
    }
  });
});

purchaseRouter.get('/download/report/pdf', function(req, res, next){
  const username = req.query.username;
  from = req.query.from;
  to = req.query.to;
  category = req.query.category;
  subCategory = req.query.subCategory;
  supplier = req.query.supplier;
  PurchaseHistory.getInDateRange(from, to, category, subCategory, supplier, function(err, rows){
    if(err){
      console.error(err);
    }else{
      User.location(username, function(err, location) {
        var fonts = {
          Roboto: {
            normal: 'fonts/Roboto-Regular.ttf',
            bold: 'fonts/Roboto-Medium.ttf',
            italics: 'fonts/Roboto-Italic.ttf',
            bolditalics: 'fonts/Roboto-MediumItalic.ttf'
          }
        };
        
        var printer = new PdfPrinter(fonts);
  
        var bdy = [
          [{text: 'Item Name', style: 'tableHeader', fontSize: 10}, {text: 'Brand', style: 'tableHeader', fontSize: 10}, {text: 'Category', style: 'tableHeader', fontSize: 10}, {text: 'Sub Category', style: 'tableHeader', fontSize: 10}, {text: 'Cost Price (Rs)', style: 'tableHeader', fontSize: 10}, {text: 'Quantity', style: 'tableHeader', fontSize: 10}, {text: 'Total (Rs)', style: 'tableHeader', fontSize: 10}, {text: 'Timestamp', style: 'tableHeader', fontSize: 10}]
        ]
  
        let items = 0, total = 0
        if (rows.length > 0) {
          for(var i=0; i<rows.length; i++){
            bdy.push([{text: rows[i].name, fontSize: 10},
              {text: rows[i].brand, fontSize: 10},
              {text: rows[i].category, fontSize: 10},
              {text: rows[i].sub_category, fontSize: 10},
              {text: rows[i].cost_price.toFixed(2), alignment: 'right', fontSize: 10},
              {text: parseFloat(rows[i].qty.toFixed(3)), alignment: 'right', fontSize: 10},
              {text: rows[i].total.toFixed(2), alignment: 'right', fontSize: 10},
              {text: rows[i].time_stamp.toISOString().slice(0, 10), fontSize: 10}]);
            items = items + 1;
            total = total + rows[i].total;
          }
        } else {
          bdy.push(["no data", "no data", "no data", "no data", "no data", "no data", "no data", "no data"]);
        }
  
        var bdy2 = [
          [{text: 'Summary', style: 'tableHeader'}, ""],
          ["No. of Items", {text: items, alignment: 'right'}],
          ["Total", {text: total.toFixed(2), alignment: 'right'}]
        ]
  
        var docDefinition = {
          content: [
            {
              text: 'Purchasing Report of '+location[0]['name'],
              style: 'header',
              margin: [ 0, 0, 0, 10]
            },
            'Generated on: '+new Date(),
            'Address: '+location[0]["address"],
            'Email: '+location[0]["email"],
            {text: 'Contact No: '+location[0]["contact_number"], margin: [ 0, 0, 0, 10]},
            {
              style: 'tableExample',
              table: {
                headerRows: 1,
                widths: [96, 52, 52, 52, 43, 47, 43, 53],
                body: bdy
              },
              layout: {
                hLineWidth: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 2 : 1;
                },
                vLineWidth: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 2 : 1;
                },
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
                }
              }
            },
            {
              style: 'tableExample',
              table: {
                headerRows: 1,
                body: bdy2
              },
              layout: {
                hLineWidth: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 2 : 1;
                },
                vLineWidth: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 2 : 1;
                },
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
                }
              }
            }
          ],
          styles: {
            header: {
              fontSize: 15,
              bold: true
            },
            tableExample: {
              margin: [0, 5, 0, 15]
            },
            tableHeader: {
              bold: true,
              fontSize: 13,
              color: 'black'
            }
          }
        };
  
        // Building the PDF
        var pdfDoc = printer.createPdfKitDocument(docDefinition);
        let chunks = [];
  
        pdfDoc.on('data', (chunk) => {
          chunks.push(chunk);
        });
      
        pdfDoc.on('end', () => {
          const result = Buffer.concat(chunks);
          res.setHeader('Content-Type', 'application/pdf');
          res.send(result);
        });
        
        pdfDoc.end();
      });
    }
  });
});

module.exports = purchaseRouter;