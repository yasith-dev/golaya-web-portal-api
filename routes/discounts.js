var express = require('express');
var router = express.Router();
var Sales = require('../model/Sales');
var SalesItem = require("../model/SalesItem");
var User = require('../model/User');
var commonUtil = require('../common/util');
var PdfPrinter = require('pdfmake');
var fs = require('fs');
const AuthVerify = require('../authentication/authVerify');

router.get('/', AuthVerify, function(req, res, next){
  from = req.query.from;
  to = req.query.to;
  Sales.getDiscounted(from, to, function(err, rows){
    if(err){
      console.error(err);
    }else{
      var resp = {
        rows: rows
      };
      res.send(resp);
    }
  });
});

router.get('/download/invoice-discount-report/excel', function(req, res, next){
  const username = req.query.username;
  from = req.query.from;
  to = req.query.to;
  Sales.getDiscounted(from, to, function(err, rows){
    if(err){
      console.error(err);
    }else{
      User.location(username, function(err, location) {
        var workbook = commonUtil.getEmptyExcel();
        sheet = workbook.addWorksheet('Discounts by Invoice');
        file = commonUtil.getTempFile('discounts-by-invoice-report-','.xlsx');

        sheet.columns = [
          {id: 'invoice_no', width: 30},
          {id: 'discount_percentage', width: 20},
          {id: 'discount', width: 20},
          {id: 'discount_username', width: 30}
        ];

        // set title
        sheet.mergeCells('A2:D2');
        sheet.getCell('A2').value = "Discounts by Invoice Report of "+location[0]['name'];
        sheet.getCell('A2').font = {size: 14, name:'Calibri', bold:true};
        sheet.getCell('A2').alignment = {vertical:'middle'};
        sheet.getRow(2).height = 30;
        sheet.addRow(2).values = ['Generated on:',new Date()];
        sheet.addRow(2).values = ['Address:', location[0]['address']];
        sheet.addRow(2).values = ['Email:',location[0]['email']];
        sheet.addRow(2).values = ['Contact No', location[0]['contact_number']];
        sheet.addRow(4);

        // set column headers
        sheet.addRow(4).values = ['Invoice No.','Discount %','Discount Amount (Rs)','Issued By'];
        sheet.getRow(8).font = {bold:true, name:'Calibri'};

        for(var i=0; i<rows.length; i++){
          var row = rows[i];
          var discount_percentage = '-';
          var discount = 0;
          if(row.discount_is_percentage === 1){
            discount_percentage = row.discount_amount;
            discount = (row.sub_total - row.items_total_discount) * discount_percentage / 100;
          }else{
            discount = row.discount_amount;
          }
          var data_row = [row.invoice_no, discount_percentage.toFixed(2), discount.toFixed(2), row.discount_username]

          sheet.addRow(data_row);
        }

        workbook.xlsx.writeFile(file.name).then(function(){
          console.log('created file : '+file.name);
          res.sendFile(file.name,function(err){
          });
        });
      });
    }
  });
});

router.get('/download/invoice-discount-report/pdf', function(req, res, next){
  const username = req.query.username;
  from = req.query.from;
  to = req.query.to;
  Sales.getDiscounted(from, to, function(err, rows){
    if(err){
      console.error(err);
    }else{
      User.location(username, function(err, location) {
        var fonts = {
          Roboto: {
            normal: 'fonts/Roboto-Regular.ttf',
            bold: 'fonts/Roboto-Medium.ttf',
            italics: 'fonts/Roboto-Italic.ttf',
            bolditalics: 'fonts/Roboto-MediumItalic.ttf'
          }
        };
        
        var printer = new PdfPrinter(fonts);
  
        var bdy = [
          [{text: 'Invoice No.', style: 'tableHeader'}, {text: 'Discount %', style: 'tableHeader'}, {text: 'Discount Amount (Rs)', style: 'tableHeader'}, {text: 'Issued By', style: 'tableHeader'}]
        ]
  
        if (rows.length > 0) {
          for(var i=0; i<rows.length; i++){
            var discount_percentage = '-';
            var discount = 0;
            if(rows[i].discount_is_percentage === 1){
              discount_percentage = rows[i].discount_amount;
              discount = (rows[i].sub_total - rows[i].items_total_discount) * discount_percentage / 100;
            }else{
              discount = rows[i].discount_amount;
            }
            bdy.push([rows[i].invoice_no, {text: discount_percentage.toFixed(2), alignment: 'right'}, {text: discount.toFixed(2), alignment: 'right'}, rows[i].discount_username]);
          }
        } else {
          bdy.push(["no data", "no data", "no data", "no data"]);
        }
  
        
  
        var docDefinition = {
          content: [
            {
              text: 'Discounts by Invoice Report of '+location[0]['name'],
              style: 'header',
              margin: [ 0, 0, 0, 10 ]
            },
            'Generated on: '+new Date(),
            'Address: '+location[0]["address"],
            'Email: '+location[0]["email"],
            {text: 'Contact No: '+location[0]["contact_number"], margin: [ 0, 0, 0, 10 ]},
            {
              style: 'tableExample',
              table: {
                headerRows: 1,
                body: bdy
              },
              layout: {
                hLineWidth: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 2 : 1;
                },
                vLineWidth: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 2 : 1;
                },
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
                }
              }
            }
          ],
          styles: {
            header: {
              fontSize: 15,
              bold: true
            },
            tableExample: {
              margin: [0, 5, 0, 15]
            },
            tableHeader: {
              bold: true,
              fontSize: 13,
              color: 'black'
            }
          }
        };
  
        // Building the PDF
        var pdfDoc = printer.createPdfKitDocument(docDefinition);
        let chunks = [];
  
        pdfDoc.on('data', (chunk) => {
          chunks.push(chunk);
        });
      
        pdfDoc.on('end', () => {
          const result = Buffer.concat(chunks);
          res.setHeader('Content-Type', 'application/pdf');
          res.send(result);
        });
        
        pdfDoc.end();
      });
    }
  });
});

router.get('/download/item-discount-report', function(req, res, next){
  SalesItem.getDiscountedItems(function(err, rows){
    if(err){
      console.error(err);
    }else{
      var workbook = commonUtil.getEmptyExcel();
      sheet = workbook.addWorksheet('Item Discount Report');
      file = commonUtil.getTempFile('item-discount-report-','.xlsx');

      sheet.columns = [
        {header: 'Item Name', key: 'item_name'},
        {header: 'Discount %', key: 'discount_amount'},
        {header: 'Discount Amount (Rs)', key: 'invoice_no'},
        {header: 'Issued By', key: 'discount_user'}
      ];

      for(var i=0; i<rows.length; i++){
        var row = rows[i];

        var discount_percentage = '-';
        var discount = 0;
        if(row.discount_is_percentage === 1){
          discount_percentage = row.discount_amount;
          discount = row.selling_price * discount_percentage / 100;
        }else{
          discount = row.discount_amount;
        }
        var data_row = [row.item_name, discount_percentage, discount, row.discount_username];

        sheet.addRow(data_row);
      }

      workbook.xlsx.writeFile(file.name).then(function(){
        console.log('created file : '+file.name);
        res.sendFile(file.name,function(err){
        });
      });
    }
  });
});

module.exports = router;
