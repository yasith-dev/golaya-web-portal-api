var express = require("express");
var router = express.Router();
var Stock = require('../model/Stock');
var User = require('../model/User');
var commonUtil = require('../common/util');
var PdfPrinter = require('pdfmake');
var fs = require('fs');
const AuthVerify = require('../authentication/authVerify');

router.get('/all', AuthVerify, (req, res, next) => {
  category = req.query.category;
  subCategory = req.query.subCategory;
  supplier = req.query.supplier;
  expire = req.query.expire;
  Stock.getAll(category, subCategory, supplier, expire, (err, rows) => {
    if (err) {
      console.error(err);
    } else {
      var resp = {
        sales: rows
      };
      res.send(resp);
    }
  });

});

router.get('/lowstock', AuthVerify, function (req, res, next) {
  Stock.getReorderItems(function (err, rows) {
    if (err) {
      console.error(err);
    } else {
      res.json({
        sales: rows
      });
    }
  });
});

router.get('/download/report/excel', function (req, res, next) {
  const username = req.query.username;
  category = req.query.category;
  subCategory = req.query.subCategory;
  supplier = req.query.supplier;
  expire = req.query.expire;
  Stock.getAll(category, subCategory, supplier, expire, (err, rows) => {
    if (err) {
      console.error(err);
    } else {
      User.location(username, function(err, location) {
        var workbook = commonUtil.getEmptyExcel();
        sheet = workbook.addWorksheet('Sales Item Report');
        file = commonUtil.getTempFile('sales-item-report-', '.xlsx');

        sheet.columns = [{
            id: 'item_name', width: 30
          },
          {
            id: 'item_brand', width: 20
          },
          {
            id: 'category', width: 20
          },
          {
            id: 'sub_category', width: 20
          },
          {
            id: 'cost_price', width: 20
          },
          {
            id: 'selling_price', width: 20
          },
          {
            id: 'qty', width: 20
          },
          {
            id: 'alert_qty', width: 20
          },
          {
            id: 're_order_qty', width: 20
          }
        ];

        // set title
        sheet.mergeCells('A2:D2');
        sheet.getCell('A2').value = "Stock Report of "+location[0]['name'];
        sheet.getCell('A2').font = {size: 14, name:'Calibri', bold:true};
        sheet.getCell('A2').alignment = {vertical:'middle'};
        sheet.getRow(2).height = 30;
        sheet.addRow(2).values = ['Generated on:',new Date()];
        sheet.addRow(2).values = ['Address:', location[0]['address']];
        sheet.addRow(2).values = ['Email:',location[0]['email']];
        sheet.addRow(2).values = ['Contact No', location[0]['contact_number']];
        sheet.addRow(4);

        // set column headers
        sheet.addRow(4).values = ['Item','Brand','Category','Sub Category', 'Cost Price (Rs)', 'Selling Price (Rs)', 'Quantity', 'Alert Quantity', 'Reorder Quantity'];
        sheet.getRow(8).font = {bold:true, name:'Calibri'};

        for (var i = 0; i < rows.length; i++) {
          var row = rows[i];
          var report_row = [row.item_name, row.item_brand, row.category, row.sub_category,
            row.cost_price.toFixed(2), row.selling_price.toFixed(2), parseFloat(row.qty.toFixed(3)), parseFloat(row.alert_qty.toFixed(3)), parseFloat(row.re_order_qty.toFixed(3))
          ];
          sheet.addRow(report_row);
        }

        workbook.xlsx.writeFile(file.name).then(function(){
          console.log('created file : '+file.name);
          res.sendFile(file.name,function(err){
          });
        });
      });
    }
  });
});

router.get('/download/report/pdf', function(req, res, next){
  const username = req.query.username;
  category = req.query.category;
  subCategory = req.query.subCategory;
  supplier = req.query.supplier;
  expire = req.query.expire;
  Stock.getAll(category, subCategory, supplier, expire, (err, rows) => {
    if(err){
      console.error(err);
    }else{
      User.location(username, function(err, location) {
        var fonts = {
          Roboto: {
            normal: 'fonts/Roboto-Regular.ttf',
            bold: 'fonts/Roboto-Medium.ttf',
            italics: 'fonts/Roboto-Italic.ttf',
            bolditalics: 'fonts/Roboto-MediumItalic.ttf'
          }
        };
        
        var printer = new PdfPrinter(fonts);
  
        var bdy = [
          [{text: 'Item',style: 'tableHeader', fontSize: 10},
          {text: 'Brand', style: 'tableHeader', fontSize: 10},
          {text: 'Category', style: 'tableHeader', fontSize: 10},
          {text: 'Sub Category', style: 'tableHeader', fontSize: 10},
          {text: 'Cost Price (Rs)', style: 'tableHeader', fontSize: 10},
          {text: 'Selling Price (Rs)', style: 'tableHeader', fontSize: 10},
          {text: 'Qty', style: 'tableHeader', fontSize: 10},
          {text: 'Alert Qty', style: 'tableHeader', fontSize: 10},
          {text: 'Reorder Qty', style: 'tableHeader', fontSize: 10}]
        ]
  
        if (rows.length > 0) {
          for(var i=0; i<rows.length; i++){
            bdy.push([{text: rows[i].item_name, fontSize: 9},
              {text: rows[i].item_brand, fontSize: 9},
              {text: rows[i].category, fontSize: 9},
              {text: rows[i].sub_category, fontSize: 9},
              {text: rows[i].cost_price.toFixed(2), alignment: 'right', fontSize: 9},
              {text: rows[i].selling_price.toFixed(2), alignment: 'right', fontSize: 9},
              {text: parseFloat(rows[i].qty.toFixed(3)), alignment: 'right', fontSize: 9},
              {text: parseFloat(rows[i].alert_qty.toFixed(3)), alignment: 'right', fontSize: 9},
              {text: parseFloat(rows[i].re_order_qty.toFixed(3)), alignment: 'right', fontSize: 9}]);
          }
        } else {
          bdy.push(["no data", "no data", "no data", "no data", "no data", "no data", "no data", "no data", "no data"]);
        }
  
        
  
        var docDefinition = {
          content: [
            {
              text: 'Purchasing Report of '+location[0]['name'],
              style: 'header',
              margin: [ 0, 0, 0, 10 ]
            },
            'Generated on: '+new Date(),
            'Address: '+location[0]["address"],
            'Email: '+location[0]["email"],
            {text: 'Contact No: '+location[0]["contact_number"], margin: [ 0, 0, 0, 10 ]},
            {
              style: 'tableExample',
              table: {
                headerRows: 1,
                widths: [84, 45, 45, 45, 42, 42, 44, 44, 44],
                body: bdy
              },
              layout: {
                hLineWidth: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 2 : 1;
                },
                vLineWidth: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 2 : 1;
                },
                hLineColor: function (i, node) {
                  return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
                },
                vLineColor: function (i, node) {
                  return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
                }
              }
            }
          ],
          styles: {
            header: {
              fontSize: 15,
              bold: true
            },
            tableExample: {
              margin: [0, 5, 0, 15]
            },
            tableHeader: {
              bold: true,
              fontSize: 13,
              color: 'black'
            }
          }
        };
  
        // Building the PDF
        var pdfDoc = printer.createPdfKitDocument(docDefinition);
        let chunks = [];
  
        pdfDoc.on('data', (chunk) => {
          chunks.push(chunk);
        });
      
        pdfDoc.on('end', () => {
          const result = Buffer.concat(chunks);
          res.setHeader('Content-Type', 'application/pdf');
          res.send(result);
        });
        
        pdfDoc.end();
      });
    }
  });
});

router.get('/expire', AuthVerify, (req, res, next) => {
  Stock.getExpireItems((err, rows) => {
    if (err) {
      console.error(err);
    } else {
      res.json({
        sales: rows
      });
    }
  });
});

module.exports = router;