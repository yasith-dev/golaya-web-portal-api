var express = require('express');
var jwt = require('jsonwebtoken');
var config = require('../config');

var router = express.Router();
var User = require('../model/User');

router.post('/login', function(req, res, next){
  const username = req.body.username;
  const password = req.body.password;
  console.log('recieved login request {'+username+','+password+'}');
  User.findOne(username,password,function(err,rows){
    if(err){
      console.log(err);
      return res.sendStatus(401);
    }else{
      if(rows.length > 0){
        User.updateLogin(username, function(err, res){});
        var token = jwt.sign({userID: username}, config.secret , {expiresIn: '1h'});
        res.send({token:token});
      }else{
        return res.send({error:"Username or Password is Incorrect! Please try again."})
      }
    }
  });
});

module.exports=router;