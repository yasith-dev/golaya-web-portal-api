var express = require('express');
var router = express.Router();
const AuthVerify = require('../authentication/authVerify');

/* GET home page. */
router.get('/', AuthVerify, function(req, res, next) {
  res.json({
    'msg':'hello'
  });
});

module.exports = router;