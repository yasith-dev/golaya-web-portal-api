var express = require("express");
var router = express.Router();
var SalesItem = require("../model/SalesItem");
var User = require('../model/User');
var commonUtil = require("../common/util");
var PdfPrinter = require('pdfmake');
var fs = require('fs');
const AuthVerify = require('../authentication/authVerify');

router.get('/', AuthVerify, function(req, res, next){
  from = req.query.from;
  to = req.query.to;
  category = req.query.category;
  subCategory = req.query.subCategory;
  SalesItem.getInRange(from, to, category, subCategory, function(err, rows){
    if(err){
      console.error(err);
    }else{
      var resp = {
        sales: rows
      };
      res.send(resp);
    }
  });
});

router.get('/category', AuthVerify, function(req, res, next){
  SalesItem.getCategories(function(err, rows){
    if(err){
      console.error(err);
    }else{
      var resp = [
        rows
      ];
      res.send(resp);
    }
  });
});

router.get('/sub-category', AuthVerify, function(req, res, next){
  category = req.query.category;
  SalesItem.getSubCategories(category, function(err, rows){
    if(err){
      console.error(err);
    }else{
      var resp = [
        rows
      ];
      res.send(resp);
    }
  });
});

router.get('/supplier', AuthVerify, function(req, res, next){
  SalesItem.getSuppliers(function(err, rows){
    if(err){
      console.error(err);
    }else{
      var resp = [
        rows
      ];
      res.send(resp);
    }
  });
});

router.get('/discounted-items', AuthVerify, function(req, res, next){
  SalesItem.getDiscountedItems(function(err, rows){
    if(err){
      console.error(err);
    }else{
      var resp = {
        sales: rows
      };
      res.send(resp);
    }
  });
});

router.get('/download/report/excel', function(req, res, next){
  const username = req.query.username;
  from = req.query.from;
  to = req.query.to;
  category = req.query.category;
  subCategory = req.query.subCategory;

  from = from == 'undefined' ? false : from;
  to = to == 'undefined' ? false : to;
  SalesItem.getInRange(from, to, category, subCategory, function(err, rows){
    if(err){
      console.error(err);
    }else{
      User.location(username, function(err, location) {
        if (err) {
          console.error(err);
        } else {
          var workbook = commonUtil.getEmptyExcel();
          var options = {
            dateFormat: 'DD/MM/YYYY HH:mm:ss'
          };
          sheet = workbook.addWorksheet('Sales Item Report');

          sheet.columns = [
            { id: 'item_name', width: 30 },
            { id: 'category', width: 30 },
            { id: 'sub_category', width: 30 },
            { id: 'qty', width: 30 }
          ];

          // set title
          sheet.mergeCells('A2:D2');
          sheet.getCell('A2').value = "Sales Item Report of "+location[0]['name'];
          sheet.getCell('A2').font = {size: 14, name:'Calibri', bold:true};
          sheet.getCell('A2').alignment = {vertical:'middle'};
          sheet.getRow(2).height = 30;
          sheet.addRow(2).values = ['Generated on:',new Date()];
          sheet.addRow(2).values = ['Address:', location[0]['address']];
          sheet.addRow(2).values = ['Email:',location[0]['email']];
          sheet.addRow(2).values = ['Contact No', location[0]['contact_number']];
          sheet.addRow(4);

          // set column headers
          sheet.addRow(4).values = ['Item','Category','Sub Category','Sold Quantity'];
          sheet.getRow(8).font = {bold:true, name:'Calibri'};

          // add rows
          for(var i=0; i<rows.length; i++){
            var row = rows[i];
            var temp_data_row = [row.item_name, row.category, row.sub_category, parseFloat(row.qty.toFixed(3))];
            sheet.addRow(temp_data_row);
          }

          file = commonUtil.getTempFile('sales-item-report-','.xlsx');  
          workbook.xlsx.writeFile(file.name,options).then(function(){
            console.log('created file : '+file.name);
            res.sendFile(file.name,function(err){
            });
          });
        }
      });
      
    }
  });
});

router.get('/download/report/pdf', function(req, res, next){
  const username = req.query.username;
  from = req.query.from;
  to = req.query.to;
  category = req.query.category;
  subCategory = req.query.subCategory;
  from = from == 'undefined' ? false : from;
  to = to == 'undefined' ? false : to;
  SalesItem.getInRange(from, to, category, subCategory, function(err, rows){
    if(err){
      console.error(err);
    }else{
      User.location(username, function(err, location) {
        if(err){
          console.error(err);
        }else{
          
          var fonts = {
            Roboto: {
              normal: 'fonts/Roboto-Regular.ttf',
              bold: 'fonts/Roboto-Medium.ttf',
              italics: 'fonts/Roboto-Italic.ttf',
              bolditalics: 'fonts/Roboto-MediumItalic.ttf'
            }
          };
          
          var printer = new PdfPrinter(fonts);
    
          var bdy = [
            [{text: 'Item',style: 'tableHeader'},
            {text: 'Category', style: 'tableHeader'},
            {text: 'Sub Category', style: 'tableHeader'},
            {text: 'Sold Quantity', style: 'tableHeader'}]
          ];
    
          if (rows.length > 0) {
            for(var i=0; i<rows.length; i++){
              bdy.push([
                rows[i].item_name,
                rows[i].category,
                rows[i].sub_category,
                {text: parseFloat(rows[i].qty.toFixed(3)), alignment: 'right'}]);
            }
          } else {
            bdy.push(["no data", "no data", "no data", "no data"]);
          }
    
          var docDefinition = {
            content: [
              {
                text: 'Sales Item Report of '+location[0]['name'],
                style: 'header',
                margin: [ 0, 0, 0, 10 ]
              },
              'Generated on: '+new Date(),
              'Address: '+location[0]["address"],
              'Email: '+location[0]["email"],
              {text: 'Contact No: '+location[0]["contact_number"], margin: [ 0, 0, 0, 10 ]},
              {
                style: 'tableExample',
                table: {
                  headerRows: 1,
                  body: bdy
                },
                layout: {
                  hLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 2 : 1;
                  },
                  vLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 2 : 1;
                  },
                  hLineColor: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
                  },
                  vLineColor: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
                  }
                }
              }
            ],
            styles: {
              header: {
                fontSize: 15,
                bold: true
              },
              tableExample: {
                margin: [0, 5, 0, 15]
              },
              tableHeader: {
                bold: true,
                fontSize: 13,
                color: 'black'
              }
            }
          };
    
          // Building the PDF
          var pdfDoc = printer.createPdfKitDocument(docDefinition);
          let chunks = [];
    
          pdfDoc.on('data', (chunk) => {
            chunks.push(chunk);
          });
        
          pdfDoc.on('end', () => {
            const result = Buffer.concat(chunks);
            res.setHeader('Content-Type', 'application/pdf');
            res.send(result);
          });
          
          pdfDoc.end();
        }
      });
    }
  });
});

module.exports = router;