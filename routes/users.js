var express = require('express');
var router = express.Router();
const AuthVerify = require('../authentication/authVerify');
var db = require('../db/db_connection');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/username/availability', (req, res, next) => {
  let sql = "SELECT id FROM online_user WHERE username = ?";
  const values = [
      req.query.username
  ];
  db.query(sql, values, (err, result) => {
      if (err) throw err;
      console.log(result)
      if (typeof result[0] === "undefined") {
          result = false;
      } else {
          result = true;
      }
      res.status(200).json(
          result
      );
  });
});

router.get('/list', (req, res, next) => {
  let sql = "SELECT id, name, username, mobile, user_type FROM online_user WHERE active = 1";

  db.query(sql, (err, result) => {
      if (err) throw err;
      console.log(result);
      res.status(200).json(
          result
      );
  });
});

router.get('/location', (req, res, next) => {
  let sql = "SELECT id, name FROM location";

  db.query(sql, (err, result) => {
      if (err) throw err;
      console.log(result);
      res.status(200).json(
          result
      );
  });
});

router.post('/', (req, res, next) => {
  let sql = "INSERT INTO online_user (`name`, `last_name`, `nic_passport`, `gender`, `address`, `mobile`, `username`, `email`, `password`, `key`, `location_id`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, aes_encrypt(?, 'th1sisTh3jwtS3cr37'), ?, ?)";
  const values = [
      req.body.name,
      req.body.lastname,
      req.body.nic,
      req.body.gender,
      req.body.address,
      req.body.contactNo,
      req.body.username,
      req.body.email,
      req.body.password,
      "test",
      req.body.location
  ];
  db.query(sql, values, (err, result) => {
      if (err) throw err;
      res.status(201).json(
         1
      );
  });
});

router.put('/status', (req, res, next) => {
  const values = [
      req.query.id
  ];
  let sql = "UPDATE online_user SET active = 0 WHERE id = ?";

  db.query(sql, values, (err, result) => {
      if (err) throw err;
      console.log(result);
      res.status(200).json(
          "success"
      );
  });
});

router.put('/logout', (req, res, next) => {
  let sql = "UPDATE online_user SET last_logout = now() WHERE username = ?";
  const values = [
      req.query.username
  ];
  db.query(sql, values, (err, result) => {});
});

module.exports = router;
