const db = require('../db/db_connection');

const PurchaseHistory = {
  getAll: function(callback){
    return db.query('SELECT ph.*,s.name AS supplier_name FROM purchase_history ph JOIN supplier s ON ph.supplier=s.id', callback);
  },

  getInDateRange: function(from, to, category, subCategory, supplier, callback){
    from = (from == 'undefined') ? false : from;
    to = (to == 'undefined') ? false : to;

    if(from && to){
      if(from == to){
        from = from+' 00:00:00';
        to = to+' 23:59:59';
      }
      if (category == "") {
        category = null;
      }
      if (subCategory == "") {
        subCategory = null;
      }
      if (supplier == "") {
        supplier = null;
      }
      console.log("supplier="+supplier)
      return db.query('SELECT ph.*,s.name AS supplier_name FROM purchase_history ph JOIN supplier s ON ph.supplier=s.id WHERE time_stamp >= ? AND time_stamp <= ? AND (? is null or ph.category = ?) AND (? is null or ph.sub_category = ?) AND (? is null or supplier = ?)',[from,to, category, category, subCategory, subCategory, supplier, supplier],callback);
    }else if(from){
      return db.query('SELECT ph.*,s.name AS supplier_name FROM purchase_history ph JOIN supplier s ON ph.supplier=s.id WHERE time_stamp>=?',[from],callback);
    }else if(to){
      return db.query('SELECT ph.*,s.name AS supplier_name FROM purchase_history ph JOIN supplier s ON ph.supplier=s.id WHERE time_stamp <=?',[to],callback);
    }else{
      return db.query('SELECT ph.*,s.name AS supplier_name FROM purchase_history ph JOIN supplier s ON ph.supplier=s.id WHERE day_end=0',callback);
    }
  },

  getInDateRangeGraphData:function(from, to, category, subCategory, supplier, callback) {
    from = (from == 'undefined') ? false : from;
    to = (to == 'undefined') ? false : to;

    if (category == "") {
      category = null;
    }
    if (subCategory == "") {
      subCategory = null;
    }
    if (supplier == "") {
      supplier = null;
    }

    if(from && to){
      from = from + ' 00:00:00';
      to = to + ' 23:59:59';
      return db.query('SELECT sum(total) AS total, time_stamp AS date FROM purchase_history WHERE `time_stamp` >= ? AND `time_stamp` <= ? AND (? is null or category = ?) AND (? is null or sub_category = ?) AND (? is null or supplier = ?) GROUP BY DATE(time_stamp)',[from,to, category, category, subCategory, subCategory, supplier, supplier],callback);
    }
  },

  getLastMonthPurchaseValue: function(callback){
    return db.query('SELECT SUM(total) AS total FROM purchase_history '+
    'WHERE time_stamp BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()',callback);
  }
};

module.exports = PurchaseHistory;