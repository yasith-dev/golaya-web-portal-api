var db = require("../db/db_connection");

var Stock = {
  getAll: function (category, subCategory, supplier, expire, callback) {
    if (category == "") {
      category = null;
    }
    if (subCategory == "") {
      subCategory = null;
    }
    if (supplier == "") {
      supplier = null;
    }

    if (expire == 1) {
      console.log("expire true: "+expire);
    return db.query(
      'SELECT i.*, s.name as supplier_name FROM item i '+
      'LEFT JOIN purchase_history p ON i.id=p.item_id LEFT JOIN supplier s ON p.supplier=s.id '+
      'WHERE (? is null or p.category = ?) AND (? is null or p.sub_category = ?) AND (? is null or p.supplier = ?) AND i.expiry_date IS NOT NULL '+
      'AND (i.expiry_date BETWEEN CURDATE() AND CURDATE() + INTERVAL 30 DAY) ORDER BY i.expiry_date ASC', [category, category, subCategory, subCategory, supplier, supplier], callback);
    } else {
      console.log("expire false: "+expire);
      return db.query(
        'SELECT i.*, s.name as supplier_name FROM item i '+
        'LEFT JOIN purchase_history p ON i.id=p.item_id LEFT JOIN supplier s ON p.supplier=s.id WHERE (? is null or p.category = ?) AND (? is null or p.sub_category = ?) AND (? is null or p.supplier = ?)', [category, category, subCategory, subCategory, supplier, supplier], callback);
    }
    
  },

  getReorderItems: function (callback) {
    return db.query(
      'SELECT i.*, s.name as supplier_name FROM item i '+
      'LEFT JOIN purchase_history p ON i.id=p.item_id LEFT JOIN supplier s ON p.supplier=s.id '+
      'WHERE i.qty<i.alert_qty', callback);
  },

  getExpireItems: function (callback) {
    return db.query(
      'SELECT i.*, s.name as supplier_name FROM item i '+
      'LEFT JOIN purchase_history p ON i.id=p.item_id LEFT JOIN supplier s ON p.supplier=s.id '+
      'WHERE i.expiry_date IS NOT NULL '+
      'AND (i.expiry_date BETWEEN CURDATE() AND CURDATE() + INTERVAL 30 DAY) ORDER BY i.expiry_date ASC', 
      callback
    );
  }
};

module.exports = Stock;