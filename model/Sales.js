var db = require('../db/db_connection');

var Sales = {
  getAll:function(callback){
    return db.query('SELECT * FROM sales',callback);
  },

  getTotalSales:function(date, callback){
    date = date + ' 00:00:00';
    return db.query('SELECT SUM(cost_total) AS total_cost, SUM(sub_total) AS total_sales FROM sales WHERE `timestamp` >= ?',[date],callback);
  },

  getTopItems:function(date, callback){
    date = date + ' 00:00:00';
    return db.query('SELECT item_name, SUM(sales_qty) AS qty FROM sales_items WHERE sold_time >= ? GROUP BY item_name ORDER BY SUM(sales_qty) DESC LIMIT 5',[date],callback);
  },

  getTopCategories:function(date, callback){
    date = date + ' 00:00:00';
    return db.query('SELECT category, SUM(sales_qty) AS qty FROM sales_items WHERE sold_time >= ? GROUP BY category ORDER BY SUM(sales_qty) DESC LIMIT 5',[date],callback);
  },

  getCurrent: function(callback){
    return db.query('SELECT * FROM sales WHERE day_end=0',callback);
  },

  getInDateRange:function(from, to, pType, callback){

    from = (from == 'undefined') ? false : from;
    to = (to == 'undefined') ? false : to;

    if (pType == "") {
      pType = null;
    }

    if(from && to){
      if(from == to) {
        from = from + ' 00:00:00';
        to = to + ' 23:59:59';
      }
      return db.query('SELECT * FROM sales WHERE sub_total != 0 AND grand_total != 0 AND `timestamp` >= ? AND `timestamp` <= ? AND (? is null or payment_method = ?)',[from,to, pType, pType],callback);
    }else if(from){
      console.log(from);
      return db.query('SELECT * FROM sales WHERE `timestamp` >=?',[from],callback);
    }else if(to){
      return db.query('SELECT * FROM sales WHERE `timestamp` <=?',[to],callback);
    }else{
      return db.query('SELECT * FROM sales WHERE day_end=0',callback);
    }
  },

  getInDateRangeGraphData:function(from, to, pType, callback) {
    from = (from == 'undefined') ? false : from;
    to = (to == 'undefined') ? false : to;

    if (pType == "") {
      pType = null;
    }

    if(from && to){
      from = from + ' 00:00:00';
      to = to + ' 23:59:59';
      return db.query('SELECT sum(sub_total) AS sum, timestamp AS date FROM sales WHERE sub_total != 0 AND grand_total != 0 AND `timestamp` >= ? AND `timestamp` <= ? AND (? is null or payment_method = ?) GROUP BY DATE(timestamp)',[from,to, pType, pType],callback);
    }else if(from){
      console.log(from);
      return db.query('SELECT sum(sub_total) AS sum, timestamp AS date FROM sales WHERE `timestamp` >=? GROUP BY DATE(timestamp)',[from],callback);
    }else if(to){
      return db.query('SELECT sum(sub_total) AS sum, timestamp AS date FROM sales WHERE `timestamp` <=? GROUP BY DATE(timestamp)',[to],callback);
    }else{
      return db.query('SELECT sum(sub_total) AS sum, timestamp AS date FROM sales WHERE day_end=0 GROUP BY DATE(timestamp)',callback);
    }
  },

  getDiscounted: function(from, to, callback){
    from = (from == 'undefined') ? false : from;
    to = (to == 'undefined') ? false : to;

    from = from + ' 00:00:00';
    to = to + ' 23:59:59';

    return db.query('SELECT * FROM sales WHERE discount_amount > 0 AND `timestamp` >= ? AND `timestamp` <= ?',[from, to], callback);
  },

  getDay: function(date, callback){
    from = date + ' 00:00:00';
    to = date + ' 23:59:59';
    return db.query('SELECT sum(sub_total) AS sale, sum(cost_total) AS cost, timestamp FROM sales WHERE timestamp >= ? AND timestamp <= ? GROUP BY hour(timestamp)',[from, to], callback);
  },
  getMonth: function(from, to, callback){
    from = from + ' 00:00:00';
    to = to + ' 23:59:59';
    return db.query('SELECT sum(sub_total) AS sale, sum(cost_total) AS cost, timestamp FROM sales WHERE `timestamp` >= ? AND `timestamp` <= ? GROUP BY day(timestamp)',[from, to], callback);
  },
  getYear: function(from, to, callback){
    from = from + ' 00:00:00';
    to = to + ' 23:59:59';
    return db.query('SELECT sum(sub_total) AS sale, sum(cost_total) AS cost, timestamp FROM sales WHERE `timestamp` >= ? AND `timestamp` <= ? GROUP BY month(timestamp)',[from, to], callback);
  }

  // getInRange: function(from, to, callback){
  //   return db.query('SELECT * FROM sales WHERE ')
  // }
};

module.exports=Sales;