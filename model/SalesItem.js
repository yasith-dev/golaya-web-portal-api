var db = require("../db/db_connection");
var logger = require('../common/logger');

let inRangeQuery = 'SELECT DISTINCT '+
    't1.item_name,t1.category,t1.sub_category,t2.qty,unit FROM sales_items t1 '+
    'JOIN ('+
    'SELECT item_name,SUM(sales_qty) as qty FROM sales_items WHERE sold_time>=? AND sold_time<=? GROUP BY item_name'+
    ') as t2 '+
    'ON t1.item_name = t2.item_name WHERE (? is null or t1.category = ?) AND (? is null or t1.sub_category = ?) ORDER BY t2.qty DESC';

let gtQuery = 'SELECT DISTINCT '+
    't1.item_name,t1.category,t1.sub_category,t2.qty,unit FROM sales_items t1 '+
    'JOIN ('+
    'SELECT item_name,SUM(sales_qty) as qty FROM sales_items WHERE sold_time>=? GROUP BY item_name'+
    ') as t2 '+
    'ON t1.item_name = t2.item_name ORDER BY t2.qty DESC';

let ltQuery = 'SELECT DISTINCT '+
    't1.item_name,t1.category,t1.sub_category,t2.qty,unit FROM sales_items t1 '+
    'JOIN ('+
    'SELECT item_name,SUM(sales_qty) as qty FROM sales_items WHERE sold_time<=? GROUP BY item_name'+
    ') as t2 '+
    'ON t1.item_name = t2.item_name ORDER BY t2.qty DESC';

let defaultQuery = 'SELECT DISTINCT '+
    't1.item_name,t1.category,t1.sub_category,t2.qty,unit FROM sales_items t1 '+
    'JOIN ('+
    'SELECT item_name,SUM(sales_qty) as qty FROM sales_items WHERE day_end=0 GROUP BY item_name'+
    ') as t2 '+
    'ON t1.item_name = t2.item_name ORDER BY t2.qty DESC';

var SalesItem = {
  getInRange: function(from, to, category, subCategory, callback){
    if(from && to){
      if(from == to) {
        from = from + ' 00:00:00';
        to = to + ' 23:59:59';
      }
      if (category == "") {
        category = null;
      }
      if (subCategory == "") {
        subCategory = null;
      }
      logger.info('getInRange,range,'+from+','+to);
      return db.query(inRangeQuery,[from,to, category, category, subCategory, subCategory],callback);
    }else if(from){
      logger.info('getInRange,gt,'+from);
      return db.query(gtQuery,[from],callback);
    }else if(to){
      logger.info('getInRange,lt,'+to);
      return db.query(ltQuery,[to],callback);
    }else{
      logger.info('getInRange,default,');
      return db.query(defaultQuery,callback);
    }
  },
  getCategories: function(callback) {
    return db.query('SELECT category_name FROM category', callback)
  },
  getSubCategories: function(category, callback) {
    if (category == "") {
      category = null;
    }
    return db.query('SELECT sub_category.sub_category FROM sub_category LEFT JOIN category ON sub_category.category_id = category.id WHERE (? is null or category.category_name = ?)', [category, category], callback)
  },
  getSuppliers: function(callback) {
    return db.query('SELECT id, name FROM supplier', callback)
  }
};

module.exports = SalesItem;