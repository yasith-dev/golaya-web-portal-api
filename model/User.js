var db = require('../db/db_connection');
var config = require('../config');

var User = {
  findOne:function(userid, password, callback){
    return db.query(
      'SELECT * FROM online_user WHERE username=? AND password=AES_ENCRYPT(?,?)',
      [userid, password, config.secret],
      callback);
  },
  updateLogin:function(userid, callback){
    return db.query(
      'UPDATE online_user SET last_login = now() WHERE username = ?',
      [userid],
      callback);
  },
  location:function(username, callback) {
    return db.query(
      'SELECT location.* FROM online_user LEFT JOIN location ON online_user.location_id = location.id WHERE online_user.username = ?',
      [username],
      callback);
  }
};

module.exports=User;