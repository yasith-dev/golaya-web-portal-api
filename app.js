//var createError = require('http-errors');
var express = require('express');
//var path = require('path');
//var cookieParser = require('cookie-parser');
var logger = require('morgan');
//var cors = require('cors');
//var expressJwt = require('express-jwt');
//var paginate = require('express-paginate');
//var config = require('./config');
const app = express();
const bodyParser = require('body-parser');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var authRouter = require('./routes/auth');
var salesRouter = require('./routes/sales');
var salesItemRouter = require('./routes/sales-item');
var stockRouter = require('./routes/stocks');
var discountRouter = require('./routes/discounts');
var purchaseRouter = require('./routes/purchasing');

// view engine setup - not required since this is an API
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());

// handle CORS errors
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
        return res.status(200).json({});
    }
    next();
});

// setting routes
app.use('/', indexRouter);
app.use('/auth',authRouter);
app.use('/users', usersRouter);
app.use('/sales', salesRouter);
app.use('/sales-item',salesItemRouter);
app.use('/stocks',stockRouter);
app.use('/discounts',discountRouter);
app.use('/purchasing', purchaseRouter);

//handle unavailable routes error
app.use((req, res, next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
});

//handle other errors 
app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
      "error" : {
          "message" : error.message
      }
  });
});

module.exports = app;
