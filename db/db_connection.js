const mysql = require('mysql2');
var dbConnection = null;

if(process.env.CLEARDB_DATABASE_URL){ 
  dbConnection = mysql.createPool(process.env.CLEARDB_DATABASE_URL);
}else{
  dbConnection = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'golaya',
    port    : '3360'
  });
} 

module.exports=dbConnection;